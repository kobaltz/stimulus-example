# == Schema Information
#
# Table name: counties
#
#  id         :integer          not null, primary key
#  name       :string
#  state_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class County < ApplicationRecord
  belongs_to :state
  has_many :cities
  scope :alpha_asc, -> { order(name: :asc) }
  scope :select_collection, -> { alpha_asc.map_array }
  scope :map_array, lambda {
    map { |s| [s.name, s.id, { url: cities_path(s.state_id, s.id) }] }
  }

  scope :cities_path, lambda { |state_id, county_id|
    Rails.application.routes.url_helpers.state_county_cities_path(
      state_id: state_id,
      county_id: county_id
    )
  }
end
