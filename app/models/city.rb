# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string
#  county_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class City < ApplicationRecord
  has_many :accounts
  belongs_to :county, optional: true
  scope :alpha_asc, -> { order(name: :asc) }
  scope :select_collection, -> { alpha_asc.map { |s| [s.name, s.id] } }
end
