# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  name       :string(50)
#  abbr       :string(2)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class State < ApplicationRecord
  has_many :counties
  scope :alpha_asc, -> { order(name: :asc) }
  scope :select_collection, -> { alpha_asc.map { |s| [
    s.name, 
    s.id, 
    { url: Rails.application.routes.url_helpers.state_counties_path(s) }] }}
end
