# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_20_150415) do

  create_table "accounts", force: :cascade do |t|
    t.string "name"
    t.integer "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_accounts_on_city_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.integer "county_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["county_id"], name: "index_cities_on_county_id"
    t.index ["name", "county_id"], name: "index_cities_on_name_and_county_id", unique: true
  end

  create_table "counties", force: :cascade do |t|
    t.string "name"
    t.integer "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "state_id"], name: "index_counties_on_name_and_state_id", unique: true
    t.index ["state_id"], name: "index_counties_on_state_id"
  end

  create_table "states", force: :cascade do |t|
    t.string "name", limit: 50
    t.string "abbr", limit: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "abbr"], name: "index_states_on_name_and_abbr", unique: true
  end

end
