class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :name
      t.belongs_to :county

      t.timestamps
    end
    add_index :cities, [:name, :county_id], unique: true
  end
end
