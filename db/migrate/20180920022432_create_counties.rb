class CreateCounties < ActiveRecord::Migration[5.2]
  def change
    create_table :counties do |t|
      t.string :name
      t.belongs_to :state

      t.timestamps
    end
    add_index :counties, [:name, :state_id], unique: true
  end
end
