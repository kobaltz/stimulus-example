class CreateStates < ActiveRecord::Migration[5.2]
  def change
    create_table :states do |t|
      t.string :name, limit: 50
      t.string :abbr, limit: 2

      t.timestamps
    end
    add_index :states, [:name, :abbr], unique: true
  end
end
