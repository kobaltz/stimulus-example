Rails.application.routes.draw do
  resources :accounts
  
  resources :states do
    resources :counties do
      resources :cities
    end
  end

  root to: 'accounts#index'
end
